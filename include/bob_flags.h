/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef BOB_FLAGS_H
#define BOB_FLAGS_H

enum {
	SB_FLVERBOSE = 0,
	SB_FLLINEAR,
	SB_FLNOLINKS,
	SB_FLSIMULATE,
	SB_FLTAGSTYLE,
	SB_FLFNAMES,
	SB_FLALL,
	SB_FLTEST,
	SB_FLRTREE,
	SB_FLCPP,
	SB_FLCTAGSAPPEND,
	SB_FLTHREAD,
	SB_FLDEBUG
};

enum {
	cmd_give_structs = 1,
	cmd_file,
	cmd_kinds,
	cmd_indent,
	cmd_tags,
	cmd_the_tt,
	cmd_call_tags,
	cmd_cgrep,
	cmd_makectags,
	cmd_files
};

#define SB_FLSET(arg) (ENV->sb_flags |= (1<<arg))
#define SB_FLGET(arg) (ENV->sb_flags & (1<<arg))

#endif

