/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

#ifdef POSIX_FADV_WILLNEED
#define cache_opt __cache_opt
#else
#define POSIX_FADV_WILLNEED 0
#define POSIX_FADV_DONTNEED 0
#define cache_opt __nofadvise
#endif

extern "C" DArray * plugin_init (struct env_t *env);

void cache_file (char * file, int adv)
{
	int fd;
	
	fd = open (file, O_RDONLY);
	if (fd < 0)
		return;

	Dposix_fadvise (fd, 0, fdsize (fd), adv);
	close (fd);
	return;
}

char __no_fadvise (DArray * d_opts, int * pos)
{
	char * S = d_opts->get (*pos);
	if (EQ (S, "--cache-load") || EQ (S, "--cache-free")) { 
		fprintf (stderr, "Sorry, your system do not support %s option.\n", S);
		return 1;
	}
	return 0;
}
	
char __cache_opt (DArray * d_opts, int * pos)
{
	DArray * files = ENV->d_files;
	char * S;
	int i;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--cache-load")) {
		for (i = 0; i < files->get_size (); i++) 
			cache_file (files->get (i), POSIX_FADV_WILLNEED);
		exit (0);
		return 1;
	}

	if (EQ (S, "--cache-free")) {
		for (i = 0; i < files->get_size (); i++)
			cache_file (files->get (i), POSIX_FADV_DONTNEED);
		exit (0);
	}

	return 0;
}

void cache_short_info ()
{
	printf ("System cache. (linux only)");
}

void cache_long_info () 
{
	printf ("System cache. (linux only)\n");
	printf ("Version: 1.0\n");
	printf ("Load files to system cache.\n");
	printf ("options: [--cache-load | --cache-free]\n");
}


DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_t * plug;

	ENV = env;
	Ret = new DArray (1);
	plug = CNEW (mod_t, 1);
	memset (plug, 0, sizeof (mod_t));
	plug->Version = strdup ("1.0");
	plug->Name = strdup ("Cache");
	plug->opt2 = cache_opt;
	plug->short_info = cache_short_info;
	plug->long_info = cache_long_info;

	ENV->listOptions->add ("--cache-load");
	ENV->listOptions->add ("--cache-free");

	Ret->add (LPCHAR (plug));
	return Ret;
}
