/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;

void ruby_create_class (int pos)
{
	char * className;
	char * S;
	int size;
	
	++pos;
	className = ENV->d_opts->get (pos);
	if (! className)
		return;
	
	printf ("class %s\n\n", className);
	printf ("\tdef initialize\n\tend\n\n");

	++pos;
	size = ENV->d_opts->get_size ();
	for (; pos < size; ++pos) {
		S = ENV->d_opts->get (pos);
		printf ("\tdef %s\n", S);
		printf ("\tend\n\n");
	}
	printf ("end\n\n");
}

char ruby_opt (DArray * d_opts, int * pos)
{
	if (! d_opts || ! pos)
		return 0;

	if (EQ (ENV->language, "Ruby") && (EQ(d_opts->get (*pos), "--new-class") 
				|| EQ(d_opts->get (*pos), "-nc"))) {
		ruby_create_class (*pos);
		exit (0);
	}

	return 0;
}

void ruby_short_info ()
{
	printf ("Ruby new class plugin.");
}

void ruby_long_info ()
{
	printf ("Ruby new class plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --ruby --new-class\n");
	printf ("Usage:  bob --ruby --new-class <classname> <class functions>\n");
}	

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	mod_t * plug;

	Ret = new DArray (1);
	plug = CNEW (mod_t, 1);
	memset (plug, 0, sizeof (mod_t));

	plug->Version = strdup ("1.0");
	plug->short_info = ruby_short_info;
	plug->long_info = ruby_long_info;
	plug->opt = ruby_opt;

	ENV->listOptions->add ("--new-class");
	ENV->listOptions->add ("-nc");

	Ret->add (LPCHAR (plug));
	return Ret;
}

