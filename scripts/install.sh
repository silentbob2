#!/bin/sh
cp ./silent_bob /usr/bin/
ln -s silent_bob /usr/bin/bob
ln -s silent_bob /usr/bin/tags
ln -s silent_bob /usr/bin/the_tt
ln -s silent_bob /usr/bin/gc_indent
ln -s silent_bob /usr/bin/bob_perl
ln -s silent_bob /usr/bin/bob_python
ln -s silent_bob /usr/bin/bob_ruby

# *nix
cp ./libsblib.so /usr/lib/

# MAC OS
cp ./libsblib.dylib /usr/lib/

