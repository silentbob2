=head1 NAME

silent_bob - The SilentBob is yet another indexing tool.

=head1 SYNOPSIS

 silent_bob [<files>] [<options>] [<function>]
 bob [<files>] [<options>] [<function>]
 tags ...
 the_tt <files>
 structs <files>

=head1 DESCRIPTION

The SilentBob is yet another indexing tool helping you to find your way through
the software source code. Although SilentBob is also able to index sources in 
many popular programming languages, currently C/C++ is the most functional back-end. Main features are: 
  - Code indexing
  - Function call traverse
  - Call backtrace 
  - File searching 
  - Fast searching in the source code 

B<silent_bob> -. 
	When started with single argument the SillentBob traverse the given function name
	up to he most outer caller. A simple to understand tree will be build on the screen.
	Both "tags" and "call_tags" files are required to perform traversal quickly.
	
	the SilentBob is also capable to handle  "tags" file by "exuberant-ctags" program,
	but it is not designed to do so. 

B<bob> - runs silent_bob  

B<tags> - alias for "silent_bob --tags"

B<the_tt> - alias for "silent_bob --the-tt"

B<structs> - alias for "silent_bob --give-structs"

=head1 OPTIONS

B<-V> show version info and exit

B<-L> specify file list to process as a text file

B<--cfiles> list all C/C++ related files (.h, .cpp etc)

B<--file> dump all function names from file

B<--give-structs> dump all C structures from file

B<--indent> indent the source code

B<--tags> show parts of referenced code

B<--make-ctags> Create "./tags" index file

B<--call-tags> Create function calls index file ("./call_tags")

B<--cgrep> Search by operation (exmpl: bob --cgrep if,var -- finds all comparisons of var)

B<--plugins-info> list available plug-in's

B<--u> Show reversed function call tree

B<--tag-style> Enforce ctags file format (for "--cgrep")

B<--depth> Set maximum traversal (tree) depth

B<-fn> Show source file names while traversing function calls

=head1 EXAMPLES

Build "ctags" file:

          bob --make-ctags

Build "call_tags" :

          bob --cfiles
	  bob -L ./cfiles --call-tags

Created "call_tags" file is compatible with Vim!

Show code of "sys_socket" and "file_operations" : 
          
	  bob --make-ctags
	  tags sys_socket file_operations

=head1 AUTHOR

Oleg Puchinin <graycardinalster@gmail.com>

