#!/usr/bin/ruby

require "fileutils"
include FileUtils

puts "Compilation ..."
system "cmake ./; make clean; make"
exit ret if $?.exitstatus != 0

if ARGV[0] == nil
  print 'installation prefix: '
  p = gets.chomp!
else
  p = ARGV[0]
end

mkdir_p "#{p}/bin"
mkdir_p "#{p}/lib"
copy "./silent_bob", "#{p}/bin/"
copy "./libsblib.so", "#{p}/lib/"

begin
  copy "./libsblib.so", "#{p}/lib/"
rescue Errno::ENOENT
  copy "./libsblib.dylib", "/#{p}/lib/"
end

ln_sf "silent_bob", "#{p}/bin/bob"
ln_sf "silent_bob", "#{p}/bin/tags"
ln_sf "silent_bob", "#{p}/bin/the_tt"
ln_sf "silent_bob", "#{p}/bin/gc_indent"
ln_sf "silent_bob", "#{p}/bin/bob_perl"
ln_sf "silent_bob", "#{p}/bin/bob_python"
ln_sf "silent_bob", "#{p}/bin/bob_ruby"

