/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <gclib/gclib_c.h>

/* 2005 */
__export DArray * Dsplit (char * STR, char *ch)
{
	DArray * ret = new DArray(128);
	char * S;
	char * buf = strdup (STR);
	char * ptr = buf;
	int ch_len;

	if (buf == NULL)
		return NULL;

	if (!STR || !ch) {
		delete ret;
		return NULL;
	}

	ch_len = strlen (ch);
	while((S = strstr (ptr, ch)) && S) {
		*S = 0;
		S += ch_len;
		ret->add (strdup (ptr));
		ptr = S;
	}

	if (strlen (ptr)) 
		ret->add (strdup (ptr));

	free (buf);
	return ret;
}

