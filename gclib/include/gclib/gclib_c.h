/*
 * (c); Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GCLIBC_H
#define DEFINE_GCLIBC_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>

#ifdef gclib_c__export
#undef gclib_c__export
#endif

#ifdef __cplusplus
#define gclib_c__export extern "C"
#else
#define gclib_c__export extern
typedef char bool;
#endif

#include "djob_t.h"
#include "dexec.h"

gclib_c__export void print_the_time (FILE * file_my);
gclib_c__export int close_pipe (int *fds);
gclib_c__export void Dfastsort_s(char ** a, long N);
gclib_c__export void Dtimer ();
gclib_c__export struct timeval *the_time ();
gclib_c__export int Dterm_one_kick (int fd);
gclib_c__export char *Dversion ();
gclib_c__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size);
gclib_c__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size);
gclib_c__export int Dselect (int FILENO, int SEC, int USEC);
gclib_c__export char * DFILE (const char * m_filename, int *rsize);
gclib_c__export struct stat * DSTAT (const char * S);
gclib_c__export struct stat * DLSTAT (const char * S);
gclib_c__export int DIONREAD (int fd);
gclib_c__export int fsize (const char * S);
gclib_c__export int fdsize (int fd);
gclib_c__export char * DFDMAP (int fd);
gclib_c__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size);
gclib_c__export char * Dread_to_eof (int fd, int *d_out_size);
gclib_c__export int move_stream (int fd_in, int fd_out);
gclib_c__export int Dnonblock(int fd);
gclib_c__export char * gc_realloc (char * PTR, int old_size, int new_size);
gclib_c__export void * memdup (void * PTR, int size);
//gclib_c__export int Dsplit(char * lpsz_String, char *ch, char ** outbuffer, int int_buffersize);
gclib_c__export char * Dstrmid(char * lpsz_string,char * param1, char * param2);
gclib_c__export char * chomp (char * S);
gclib_c__export char * DSTR (FILE * m_file);
gclib_c__export char * strchr_r (char * S, char ch, int d_len);
gclib_c__export char * strchrs (char *S, char ch, char ch2, char ch3, char ch4);
gclib_c__export char * Dstrstr_r (char *where, char * str); 
gclib_c__export char * Dstrndup (char *ptr, int n);
gclib_c__export char * Dtimestr (char * buf, int max);
gclib_c__export int Dsyms (char * from, char * to, char sym);
gclib_c__export char * Dmemchr (char *from, int n, char ch);
gclib_c__export char * Dmid_strchr (char *ptr, char *end, char ch);
gclib_c__export char * Dmid_getstr (char *buf, char *end);
gclib_c__export char * Drand_str (char * buf, int count);
gclib_c__export char * int2str (int i);
gclib_c__export char * Dprogram_read (char *EXEC, int * len_out);
gclib_c__export char * stail (char *S);
gclib_c__export char * strmov (char *buf, char * S);
gclib_c__export char * strip (char *str) ;
gclib_c__export char * strip2 (char *str) ;
gclib_c__export char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen) ;
gclib_c__export char * Dmid_memmem (char * begin, char * last, 	char * needle, size_t needlelen) ;
gclib_c__export int Dtmpfd (char *name) ;
gclib_c__export int fdclose (int * fd) ;
gclib_c__export char * fext (char *name) ;
gclib_c__export char * Dsprintf (char * fmt, ...) ;
gclib_c__export int logToFile (char * fileName, char * fmt, ...) ;
gclib_c__export int copyFile (char * sourceName, char * destName) ;

#define if_digit(S) ((*S >= '0') && (*S <= '9'))
#define if_abc(S) ((*S >= 'a' && *S <= 'z') || (*S >= 'A' && *S <= 'Z'))
#define OR(var,val1,val2) ((var == val1) || (var == val2))

#ifndef EQ
#define EQ(arga, argb) (!strcmp ((char *) arga, (char *) argb))
#endif

#ifndef NE
#define NE(arga, argb) (strcmp ((char *) arga, (char *) argb))
#endif

#ifndef chop
#define chop(arg1) arg1[strlen(arg1) - 1] = 0
#endif

#define DROP(arga) if (arga) { free (arga); arga = NULL; }
#define CNEW(arga,argb) (arga *)malloc (sizeof (arga)*(argb))

#undef gclib_c__export
#endif

