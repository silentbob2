g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/Fly.o sblib/Fly.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/Sblib.o sblib/Sblib.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/the_fly.o sblib/the_fly.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/py_tt.o sblib/py_tt.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/the_tt.o sblib/the_tt.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/TT.o sblib/TT.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/t_op2.o sblib/t_op2.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/wit.o sblib/wit.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/t_op.o sblib/t_op.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/dlist.o gclib/src/dlist.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/dsplit.o gclib/src/dsplit.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/elist.o gclib/src/elist.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/earray.o gclib/src/earray.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/djobs.o gclib/src/djobs.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/gclib.o gclib/src/gclib.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/ehash.o gclib/src/ehash.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/dhash.o gclib/src/dhash.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/darray.o gclib/src/darray.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/fs.o gclib/src/fs.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/deprecated_dsplit.o gclib/src/deprecated_dsplit.cxx
g++ -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/dheapsort.o gclib/src/dheapsort.cxx
gcc -Iinclude -Igclib/include/ -fPIC -DPIC -g3 -Wall -c -o release/gclib_c.o gclib/src/gclib_c.c
g++ release/*.o --shared -o release/libgclib.so
rm debug/*.o
rm release/*.o
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/interactive.o src/interactive.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/kinds.o src/kinds.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/opts_list.o src/opts_list.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/cgrep.o src/cgrep.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/modding.o src/modding.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/opts_funcs.o src/opts_funcs.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/ctags.o src/ctags.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/structs.o src/structs.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/tree.o src/tree.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/opts_settings.o src/opts_settings.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/file.o src/file.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/cFiles.o src/cFiles.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/indent.o src/indent.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/init.o src/init.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/main.o src/main.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/opts_kinds.o src/opts_kinds.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/callTags.o src/callTags.cxx
g++ -Iinclude -Igclib/include/  -g3 -Wall -c -o release/usage.o src/usage.cxx
g++ release/*.o -o release/silent_bob -Lrelease -ldl -lsblib
rm debug/*.o
rm release/*.o
