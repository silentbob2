/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <wit.h>

int opts_kinds (DArray * d_opts, int & i)
{
	char *S;
	int argc;

	if (! d_opts)
		return 0;

	argc = d_opts->get_size ();
	if (EQ (d_opts->get (i), "--kinds")) {
		if (++i >= argc)
			return 0;

		ENV->sb_cmd = cmd_kinds;
		ENV->d_kinds = 0;
		S = d_opts->get (i);
		while (*S) {
			switch (*S) {
				case 'a':
					ENV->d_kinds = 0xFFFF & 
						~OT::Extern & 
						~OT::Other & 
						~OT::Call & 
						~OT::Macro;
					break;
				case 'c':
					ENV->d_kinds |= OT::Class;
					break;
				case 'f':
					ENV->d_kinds |= OT::Function;
					break;
				case 's':
					ENV->d_kinds |= OT::Struct;
					break;
			}
			++S;
		}
		return 0;
	}

	return -1;
}

