/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <structs.h>
#include <the_tt.h>
#include <dbg.h>

namespace cgrep {
	
/// смещение (в строках) строки str в буфере d_buf.
int stringOffset (char ** d_buf, char *str) 
{
	char * S;
	int len = strlen (str);
	int fix = 0;;

	S = *d_buf;
	while (true) {
		if (! strncmp (S, str, len))
			break;
		if (*S == '\n')
			fix--;
		--S;
	}

	*d_buf = S;
	return fix;
}
	
int fixPosition (tt_state_t *tt, int t_op_no, DArray * exp) 
{
	int fix = 0;
	char * d_buf;
	int i;
	
	if (! tt || ! exp)
		return 0;

	d_buf = tt->fileData + tt->attachment[t_op_no].offset - 1;
	for (i = exp->get_size () - 1; i >= 0; --i) 
		fix += stringOffset (&d_buf, exp->get (i));

	return fix;
}

void print (tt_state_t * tt, DArray * d_array, DArray * d_lines) 
{
	int line;
	line = tt->attachment[ENV->t_op_no].pair_line + 1;
	line += fixPosition (tt, ENV->t_op_no, d_array);
	if (SB_FLGET (SB_FLTAGSTYLE)) 
		printf ("%s\t%s\t+%i\n", ENV->cgrep_exp,
				tt->fileName, line);
	else 
		printf ("%s +%i: %s", tt->fileName, line, d_lines->get (line-1));
	fflush (stdout);
}

bool scan (char *S, DArray * d_array) 
{
	char * one;
	int size;
	int i;

	size = d_array->get_size ();
	for (i = 0; i < size; i++) {
		one = d_array->get (i);
		S = strstr (S, one);
		if (! S)  
			break;
		S += strlen (one);
	}

	if (i == size)
		return true;
	return false;
}

int file (char * fileName) 
{
	char * d_ptr, *d_out;
	DArray * d_array;
	DArray * d_lines = NULL;
	tt_state_t * tt;
	char ch;
	
	if (ENV->cgrep_exp == NULL)
		return -1;

	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (tt_state_t));
	tt->fileName = strdup (fileName);
	
	if (THE_TT::do_tt_file (tt) == NULL) {
		free_tt_state (tt);
		return -1; // broken file 
	}
	
	d_out = tt->result;
	d_ptr = tt->result;
	ENV->t_op_no = 0;
	
	d_array = Dsplit (ENV->cgrep_exp, (char *) ",");
	if (d_array == NULL) 
		return -2;
	
	if (! SB_FLGET (SB_FLTAGSTYLE)) {
		d_lines = new DArray (1024);
		d_lines->from_file (tt->fileName);
	}

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		ENV->t_op_no++;
		if (! ch)
			break;
		if (! scan (d_out, d_array))
			continue;
		print (tt, d_array, d_lines);
	}

	if (d_lines) {
		d_lines->foreach (free);
		delete d_lines;
	}

	free_tt_state (tt);
	return 0;
}

int thread (int N)
{
	char m_buf[512];
	FILE * m_file;

	sprintf (m_buf, "%s%i", ENV->tmp_files, N);
	m_file = fopen (m_buf, "r");
	if (! m_file) 
		exit (-1);

	while (fgets (m_buf, 512, m_file)) {
		chomp (m_buf);
		file (m_buf);
	}

	fclose (m_file);
	exit (0);
	return 0;
}

int cgrep (EArray * d_files) 
{
	__djob_t * j;
	int i;

	if (! d_files)
		return -1;

	if (strchr (ENV->cgrep_exp, ' ') ||
	    strchr (ENV->cgrep_exp, '\t')) {
		fprintf (stderr, "You can't use spaces and tabs in expression.\n");
		return -1;
	}

	if (ENV->max_proc == 1) {
		for (i = 0; i < d_files->get_size (); ++i) 
			file (d_files->get (i));
		return 0;
	}

	d_files->strings_to_file (ENV->tmp_files);
	split_tmp_files ();
	for (i = 0; i < ENV->max_proc; ++i) {
		j = ENV->proc_list->fork ();
		if (j->child) 
			thread (i);
	}

	while ((j = ENV->proc_list->wait_all ()) && j)
		Dexec_done (j);

	remove_tmp_files ();
	return 0;
}

} // namespace cgrep

