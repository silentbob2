/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <wit.h>
#include <mod.h>

void dumpOptions ()
{
	char * S;
	ENV->listOptions->first ();
	while ((S = ENV->listOptions->get ())) {
		printf ("%s ", S);
		ENV->listOptions->next ();
	}
	printf ("\n");
}

int bob_options (DArray * d_opts, int & i)
{
	char * opt;
	int argc;

	if (! d_opts)
		return -1;

	opt = d_opts->get (i);
	argc = d_opts->get_size ();

	if (opt[0] != '-') {
		ENV->d_files->add (strdup (opt));
		return 0;
	}
	
	if (EQ (opt, "--help") || EQ (opt, "-h")) {
		usage ();
		exit (0);
	}
	
	if (EQ (opt, "-V") || EQ (opt, "--version")) {
		printf ("Silent Bob 2.0\n");
		exit (0);
	}
	
	if (EQ (opt, "--perl")) {
		ENV->language = strdup ("Perl");
		return 0;
	}

	if (EQ (opt, "--file")) {
		ENV->sb_cmd = cmd_file;
		return 0;
	}

	if (EQ (opt, "--files")) {
		if (builtin_language ()) { 
			ENV->sb_cmd = cmd_files;
			return 0;
		}
		return -1;
	}
	
	if (EQ (opt, "--the-tt")) {
		ENV->sb_cmd = cmd_the_tt;		
		return 0;
	}
	
	if (EQ (opt, "--structs")) {
		ENV->sb_cmd = cmd_give_structs;
		return 0;
	}
	
	if (EQ (opt, "--indent")) {
		ENV->sb_cmd = cmd_indent;
		return 0;
	}
	
	if (EQ (opt, "--tags")) {
		ENV->sb_cmd = cmd_tags;
		return 0;
	}

	if (EQ (opt, "--make-ctags") ||
			EQ (opt, "-c")) {
		ENV->sb_cmd = cmd_makectags;
		return 0;	
	}
	
	if (EQ (opt, "--call-tags") ||
			EQ (opt, "-ct")) {
		SB_FLSET (SB_FLTAGSTYLE); 
		ENV->sb_cmd = cmd_call_tags;
		return 0;
	}
	
	if (EQ (opt, "--cgrep")) {
		if (++i >= d_opts->get_size ())
			return 0;
		
		ENV->sb_cmd = cmd_cgrep;
		ENV->cgrep_exp = d_opts->get (i);
		return 0;
	}
	
	if (EQ (opt, "--tags")) {
		ENV->sb_cmd = cmd_tags;
		return 0;
	}

	if (EQ (opt, "-i")) {
		tags_interactive ();
		exit (0);
	}
	
	if (EQ (opt, "--cfiles") ||
			EQ (opt, "-f")) {
		bob_cfiles ();
		exit (0);
	}

	if (EQ (opt, "--time-test")) 
		Dtimer ();

	if (EQ (opt, "--plugins-viewer")) {
		plugins_viewer ();
		exit (0);
	}
	
	if (EQ (opt, "--options")) {
		dumpOptions ();
		exit (0);
	}
	
	if (EQ (opt, "--verbose")) {
		SB_FLSET (SB_FLVERBOSE);
		return 0;
	}
	
	if (EQ (opt, "-u")) {
		SB_FLSET (SB_FLRTREE);			
		return 0;
	}
	
	if (EQ (opt, "--linear") ||
			EQ (opt, "-l")) {
		SB_FLSET (SB_FLLINEAR);
		return 0;
	}
	
	if (EQ (opt, "-C") && i < argc) {
		++i;
		chdir (d_opts->get (i));
		return 0;
	}
	
	if (EQ (opt, "--linux")) {
		chdir ("/usr/src/linux");
		return 0;
	}
		
	if (EQ (opt, "--debug")) {
		SB_FLSET (SB_FLDEBUG);
		return 0;
	}
	
	if (EQ (opt, "--simulate")) {
		Dtimer ();
		SB_FLSET (SB_FLSIMULATE);
		return 0;
	}
		
	if (EQ (opt, "--no-links"))	{
		SB_FLSET (SB_FLNOLINKS);
		return 0;
	}

	if (EQ (opt, "-a") || EQ (opt, "--all")) {
		SB_FLSET (SB_FLALL);
		return 0;
	}
	
	if (EQ (opt, "--test") || EQ (opt, "-t")) {
		SB_FLSET (SB_FLTEST);
		return 0;
	}

	if (EQ (opt, "--ctags-append")) {
		SB_FLSET (SB_FLCTAGSAPPEND);
		return 0;
	}
	
	if (EQ (opt, "-A")) {
		if (++i >= argc)
			return 0;
		
		ENV->cgrep_A = atoi (d_opts->get (i));
		return 0;
	}

	if (EQ (opt, "-B")) {
		if (++i >= argc) 
			return 0;
		
		ENV->cgrep_B = atoi (d_opts->get (i));
		return 0;
	}

	if (EQ (opt, "-j")) {
		if (++i >= argc)
			return 0;
		ENV->max_proc = atoi (d_opts->get (i));
		return 0;
	}
	
	if (EQ (opt, "--tag-style") || EQ (opt, "-ts")) {
		SB_FLSET (SB_FLTAGSTYLE); 
		return 0;
	}
	
	if (EQ (opt, "-L") && ((i+1) < argc))	{
		++i;
		ENV->d_files->from_file (d_opts->get (i));
		ENV->d_files->foreach ((Dfunc_t) chomp);
		return 0;
	}
	
	if (EQ (opt, "--depth")) {
		if (++i >= argc) 
			return 0;
		
		ENV->d_depth = atoi (d_opts->get (i));
		return 0;
	}
		
	if (EQ (opt, "-fn")) {
		SB_FLSET (SB_FLFNAMES);
		return 0;
	}

	if (EQ (opt, "--lang")) {
		if (++i >= argc)
			return 0;
		
		ENV->language = strdup (d_opts->get (i));
		if (EQ (ENV->language, "cpp") ||
			EQ (ENV->language, "cxx") ||
			EQ (ENV->language, "c++")) {
			delete ENV->language;
			ENV->language = strdup ("C++");
			SB_FLSET (SB_FLCPP);
		}
		return 0;
	}

	if (EQ (opt, "--thread")) {
		SB_FLSET (SB_FLTHREAD);
		return 0;
	}

	return -1;
}

