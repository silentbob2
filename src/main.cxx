/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <wit.h>
#include <the_tt.h>
#include <mod.h>
#include <dbg.h>

extern "C" struct env_t *ENV;

void gop_log ()
{
	FILE * f;

	f = fopen ("./gop.log", "a");
	fprintf (f, "Files: %i\n", ENV->d_files->get_size ());
	fclose (f);
}

/* MAIN */
int main (int argc, char ** argv) 
{
	char * opt;
	int n_files = 0;
	DArray * d_opts;
	int i;

	sb_init ();

	ENV->sb_cmd = sb_prname (argv[0]);		
	if (argc == 1 && !ENV->sb_cmd) {
		usage ();
		exit (0);
	}

	ENV->d_files = new EArray (128);
	d_opts = new EArray (argc);
	ENV->d_opts = d_opts;
	for (i = 0; i < argc; i++) 
		d_opts->add (argv[i]);
	
	for (i = 1; i < d_opts->get_size (); i++) {
		opt = d_opts->get (i);
		if (opt[0] != '-' && index (opt, '=')) {
			setParam (opt);
			continue;
		}

		if (opt[0] != '-') {
			ENV->d_files->add (strdup (d_opts->get (i)));
			continue;
		}
		
		if (! bob_options (d_opts, i))
			continue;
		
		if (! opts_kinds (d_opts, i))
			continue;

		if (d_opts->get (i)[0] == '-' && !validOption (d_opts->get(i))) {
			fprintf (stderr, "unknown option : %s\n", d_opts->get (i));
			exit (1);
		}

		modding_optparse (&i, 1);
	} // for (i = 1; i < argc; i++)

	n_files = ENV->d_files->get_size ();
	for (i = 0; i < argc; i++) 
		modding_optparse (&i, 2);

	if (NE (ENV->language, "C") && NE (ENV->language, "C++")) {
		i = modding_start (ENV->sb_cmd);
		exit (i);
	}

	if (SB_FLGET (SB_FLCPP)) 
		ENV->d_kinds &= OT::Class | OT::Struct | OT::Function 
			| OT::Namespace | OT::Define;

	switch (ENV->sb_cmd) {
		case cmd_makectags:
			make_ctags (ENV->d_files);
			break;
		case cmd_indent:
			nogui_indent ();
			break;
		case cmd_cgrep:
			cgrep::cgrep (ENV->d_files);
			break;
		case cmd_call_tags:
			call_tags (ENV->d_files);
			goto out;
		case cmd_tags:
			tags (ENV->d_files, NULL);
			goto out;
	}

	if (ENV->sb_cmd == cmd_makectags ||
	    ENV->sb_cmd == cmd_indent ||
	    ENV->sb_cmd == cmd_cgrep) {
		print_the_time (NULL);
		exit (0);
	}

	for (i = 0; i < n_files; i++) {
		switch (ENV->sb_cmd) {
			case cmd_file:
				flist_main (ENV->d_files->get (i));
				break;
			case cmd_the_tt:
				THE_TT::the_tt_main (ENV->d_files->get(i));	
				break;
			case cmd_give_structs:
				got_structs (ENV->d_files->get (i));	
				break;
			case cmd_kinds:
				kindsFile (ENV->d_files->get(i), ENV->d_kinds);
				break;
		}
	}

	if (ENV->sb_cmd == cmd_file || 
			ENV->sb_cmd == cmd_the_tt || 
			ENV->sb_cmd == cmd_give_structs || 
			ENV->sb_cmd == cmd_kinds)
		goto out;

	ENV->sb_cmd = ENV->sb_cmd;
	
	if (ENV->sb_cmd == cmd_the_tt && !n_files) // THE_TT for stdin
		THE_TT::the_tt_main ((char *) "-");
	else if (ENV->sb_cmd == cmd_give_structs && !n_files) {
		got_structs ((char *) "-");
		print_the_time (NULL);
		exit (0);
	}
	
	if ((ENV->sb_cmd == cmd_kinds || ENV->sb_cmd == cmd_file) && !n_files) {
		char d_buf[1024];
	
		while (fgets (d_buf, 1023, stdin)) {
			chomp (d_buf);
			switch (ENV->sb_cmd) {
				case cmd_kinds:
					kindsFile (d_buf, ENV->d_kinds);
					break;
				case cmd_file:
					flist_main (d_buf);
					break;
			}
		}
	}
	
	if (!SB_FLGET (SB_FLRTREE))
		call_tree (ENV->d_files->get (0), 0);
	else
		reverse_calltree (ENV->d_files->get (0));

out:
	print_the_time (NULL);

	if (ENV->immune_list)
		delete ENV->immune_list;
	
	ENV->d_files->foreach (free);
	delete ENV->d_files;
	fflush (stdout);

	return EXIT_SUCCESS;
}

