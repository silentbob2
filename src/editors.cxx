/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

#define TMP_FILE "/tmp/.silent_bob.tmp.cpp"
char * editor;

void try_editor ()
{
	int pid;
	int fd;
	int status;
	char m_buf[1024];
	
	pid = fork ();
	if (pid < 0)
		return;

	if (pid == 0) {
		unlink (TMP_FILE);
		fd = open (TMP_FILE, O_WRONLY | O_CREAT);
		fchmod (fd, S_IREAD | S_IWRITE);
		if (fd == -1) {
			perror ("open");
			exit (1);
		}
		dup2 (fd, fileno (stdout));
		return;
	} else {
		if ((waitpid (pid, &status, 0) < 0) ||
			status != 0) 
			exit (status);
		
		sprintf (m_buf, "%s %s", editor, TMP_FILE);
		system (m_buf);
		exit (0);
	}
}

char editor_opt (DArray * d_opts, int * i)
{
	char * S;
	if (! d_opts || ! i)
		return 0;

	if (editor != NULL)
		return 1;

	S = d_opts->get (*i);
	if (EQ(S, "-g") || EQ(S, "--gvim")) 
		editor = (char *) "gvim";

	if (EQ(S, "--emacs"))
		editor = (char *) "emacs";

	if (EQ(S, "--nano"))
		editor = (char *) "nano";

	if (editor != NULL) {
		try_editor ();
		return 1;
	}

	return 0;
}

void short_info ()
{
	printf ("Editors.");
}

void long_info ()
{
	printf ("Editors.\n");
	printf ("Version: 1.0\n");
	printf ("Few editors for SilentBob:\n"
			"\t-g --gvim\t-\tGVim\n"
			"\t--emacs\t\t-\tEmacs\n"
			"\t--nano\t\t-\tnano\n");
}

int editors_init ()
{
	mod_t * plug;

	plug = CNEW (mod_t, 1);
	memset (plug, 0, sizeof (mod_t));
	plug->Version = strdup ("1.0");
	plug->opt = editor_opt;
	plug->Name = strdup ("Editors");
	plug->short_info = short_info;
	plug->long_info = long_info;
	plug->internal = true;

	ENV->listOptions->add (	"-g");
	ENV->listOptions->add (	"--gvim");
	ENV->listOptions->add (	"--emacs");
	ENV->listOptions->add (	"--nano");

	editor = NULL;
	ENV->modding->add (LPCHAR (plug));

	return 0;
}
