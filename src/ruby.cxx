/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

int ruby_ctags (char * f_name, FILE * of);
FILE * ruby_out_file;

void ruby_files () 
{
	unlink ("./ruby_files");
	sblib_find ("./", "*.rb", "./ruby_files");
}

char ruby_opt (DArray * d_opts, int * pos)
{
	if (! d_opts || ! pos)
		return 0;

	if (EQ (d_opts->get (*pos), "--ruby")) {
		ENV->language = strdup ("Ruby");
		return 1;
	}
	
	if (EQ (d_opts->get (*pos), "--files")) {
		ruby_files ();
		return 1;
	}

	return 0;
}

char ruby_opt2 (DArray * d_opts, int * pos)
{
	return 0;
}

bool ruby_scan_line (const char *S)
{
	char * ptr;
	if (! S)
		return false;

	ptr = strchr  (S, '#');
	if (ptr)
		*ptr = '\0';

	if  (strstr (S, "class ") ||
		       	strstr (S, "module ") ||
		       	strstr (S, "def "))
		return true;

	return false;
}

void ruby_tag (char * filename, char * str, int line, FILE * of)
{
	char * S;

	S = ruby_last_word (str);
	if (! S)
		return;

	chomp (S);
	if (! of)
		printf ("%s\t%s\t%d\n", S, filename, line);
	else
		fprintf (of, "%s\t%s\t%d\n", S, filename, line);
}

void ruby_file (char * f_name)
{
	FILE * myfile;
	char * buf;

	if (! f_name)
		return;

	myfile = fopen (f_name, "r");
	if (! myfile)
		return;

	printf ("#   %s\n", f_name);
	buf = CNEW (char, 4096);
	while (fgets (buf, 4096, myfile)) {
		if (ruby_scan_line (buf))
			printf ("%s", buf);
	}

	printf ("\n");
	fclose (myfile);
	DROP (buf);
}

void __ruby_files (char * f_name)
{
	unlink (f_name);
	sblib_find ("./", "*.rb", f_name);
}


void ruby_lookup ()
{
	int i;
	DArray * d_array;

	__ruby_files (ENV->tmp_files);
	d_array = new DArray (32);
	d_array->from_file (ENV->tmp_files);
	d_array->foreach ((Dfunc_t)chomp);

	for (i = 0; i < d_array->get_size (); ++i) {
		if (! d_array->get (i))
			continue;

		ruby_ctags (d_array->get (i), ruby_out_file);
	}
	
	unlink (ENV->tmp_files);
	d_array->foreach (free);
	delete d_array;
}

int ruby_ctags (char * f_name, FILE * of)
{
	FILE * myfile;
	char * buf;
	int line = 0;

	ruby_out_file = of;
	if (! f_name) {
		ruby_lookup ();
		return 0;
	}

	myfile = fopen (f_name, "r");
	if (! myfile)
		return -1;

	buf = CNEW (char, 4096);
	while (fgets (buf, 4096, myfile)) {
		++line;
		if (ruby_scan_line (buf))
			ruby_tag (f_name, buf, line, of);
	}

	fclose (myfile);
	DROP (buf);

	return 0;
}

void ruby_short_info ()
{
	printf ("Ruby language plugin.");
}

void ruby_long_info ()
{
	printf ("Ruby plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --ruby --files --file\n");
	printf ("Usage:  bob --ruby --files\n"
		"\tbob ./ruby_files --ruby --make-ctags\n"
		"\tbob <file> --ruby --file\n");
}	

int ruby_init ()
{
	mod_t * plug;

	plug = CNEW (mod_t, 1);
	memset (plug, 0, sizeof (mod_t));

	plug->Version = strdup ("1.0");
	plug->short_info = ruby_short_info;
	plug->long_info = ruby_long_info;
	plug->opt = ruby_opt;
	plug->opt2 = ruby_opt2;
	plug->make_ctags = ruby_ctags;
	plug->language = strdup ("Ruby");
	plug->file = ruby_file;
	plug->internal = true;

	ENV->listOptions->add ("--ruby");
	ENV->listOptions->add ("--files");
	ENV->modding->add (LPCHAR (plug));

	return 0;
}

