/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 * 25/06/06 - Perl plugin for SilentBob.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <gclib/gclib.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <the_tt.h>
#include <the_fly.hpp>

extern "C" DArray * plugin_init (struct env_t *env);
void __perl_files (char * f_name);
int perl_make_ctags (char * f_name, FILE * of);
char t_op2 (char ** d_in, char ** d_prev);

struct env_t *ENV;
FILE * perl_out_file;

void pl_sub (struct tt_state_t *tt, int line, char * d_out, char ch)
{
	char *S;
	char *part1, *part2;

	S = strchr (d_out, '(');
	if (S)
		*S = '\0';
	
	part1 = strchr (d_out, ' ');
	++part1;
	part2 = strchr (part1, ':');

	if (part2) {
		*part2 = '\0';
		++part2;
		part2 = strip (part2);
		strip2 (part2);
	}

	strip2 (part1);
	if (part2) 
		fprintf (perl_out_file, "%s:%s\t%s\t%i\n", part1, part2, tt->fileName, line);
	else
		fprintf (perl_out_file, "%s\t%s\t%i\n", part1, tt->fileName, line);
}

void pl_package (struct tt_state_t *tt, int line, char * d_out, char ch)
{
	char * S;
	char * ptr;

	S = strchr (d_out, ' ');
	if (! S)
		return;

	++S;
	strip2 (S);

	ptr = rindex (S, ':');
	if (ptr) {
		++ptr;
		fprintf (perl_out_file, "%s\t%s\t%i\n", ptr, tt->fileName, line);
	}
	fprintf (perl_out_file, "%s\t%s\t%i\n", S, tt->fileName, line);
}

void perl_make_tag (struct tt_state_t *tt, char * d_out, char ch) 
{
	int line;

	line = tt->attachment[ENV->t_op_no].pair_line+1;
	if (*d_out == ' ')
		++d_out;

	if ((ch == '{') && (! strncmp (d_out, "sub ", 4))) {
		pl_sub (tt, line, d_out, ch);
		return;
	}
	
	if ((ch == ';') && (! strncmp (d_out, "package ", 8))) {
		pl_package (tt, line, d_out, ch);
		return;
	}
}

void pl_lookup ()
{
	int i;
	DArray * d_array;

	__perl_files (ENV->tmp_files);
	d_array = new DArray (32);
	d_array->from_file (ENV->tmp_files);
	d_array->foreach ((Dfunc_t)chomp);

	for (i = 0; i < d_array->get_size (); ++i) {
		if (! d_array->get (i))
			continue;

		perl_make_ctags (d_array->get (i), perl_out_file);
	}
	
	unlink (ENV->tmp_files);
	d_array->foreach (free);
	delete d_array;
}

int perl_make_ctags (char * f_name, FILE * of) 
{
	char ch;
	char *d_ptr,*d_out; // for t_op2
	int block_depth = 0;
	struct tt_state_t *tt;

	perl_out_file = of;
	if (f_name == NULL) {
		pl_lookup ();
		return 0;
	}
	
	ENV->t_op_no = 0;
	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (struct tt_state_t));
	tt->fileName = strdup (f_name);
	THE_FLY::fly_for_file (tt);

	d_out = tt->result;
	d_ptr = d_out;
	while (true) {
		ch = t_op2 (&d_ptr, &d_out);
		ENV->t_op_no++;

		if (ch == '\0')
			break;

		if (! block_depth)
			perl_make_tag (tt, d_out, ch);

		if (ch == '{') 
			++block_depth;

		if (ch == '}')
			--block_depth;
		
		if (block_depth < 0)
			block_depth = 0;
	}
	
	fflush (perl_out_file);
	free_tt_state (tt);
	return 0;
}

int perl_call_tags (char * f_name)
{
	printf ("Under construction.\n");
	return 0;
}

char perl_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;

	if (EQ (d_opts->get (*i), "--perl")) {
		ENV->language =  (char *) "Perl";
		return 1;
	}
	return 0;
}

void __perl_files (char * f_name)
{
	unlink (f_name);
	sblib_find ("./", "*.pm", f_name);
	sblib_find ("./", "*.pl", f_name);
	sblib_find ("./", "*.ph", f_name);
}

char perl_files_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;
	
	if (EQ (d_opts->get (*i), "--files") && EQ (ENV->language, "Perl")) {
		__perl_files ((char *) "./perl_files");
		return 1;
	}

	return 0;
}

void perl_plugin_short_info ()
{
	printf ("Perl language support.");
}


void perl_plugin_long_info ()
{
	printf ("Perl language support.\n");
	printf ("Version: 1.0-rc1\n");
	printf ("options: --perl --make-ctags\n");
}

void perl_files_short_info ()
{
	printf ("Perl files.");
}

void perl_files_long_info ()
{
	printf ("Perl files.\n");
	printf ("Version: 1.0\n");
	printf ("options: --perl --files\n");
}

int perl_init ()
{
	struct mod_t * pm;
	struct mod_t * mod_perlfiles;
	struct mod_t * pm_lang;

	pm = CNEW (mod_t, 1);
	pm_lang = CNEW (mod_t, 1);
	mod_perlfiles = CNEW (mod_t, 1);
	
	memset (pm, 0, sizeof (mod_t));
	memset (pm_lang, 0, sizeof (mod_t));
	memset (mod_perlfiles, 0, sizeof (mod_t));

	pm->Version = strdup ("0.1");
	pm->opt = perl_opt;
	pm->internal = true;

	mod_perlfiles->Version = strdup ("1.0");
	mod_perlfiles->short_info = perl_files_short_info;
	mod_perlfiles->long_info = perl_files_long_info;
	mod_perlfiles->opt = perl_files_opt;
	mod_perlfiles->internal = true;
	
	pm_lang->Version = strdup ("1.0-rc1");
	pm_lang->short_info = perl_plugin_short_info;
	pm_lang->long_info = perl_plugin_long_info;
	pm_lang->language = strdup ("Perl");
	pm_lang->the = THE_FLY::fly_for_file;
	pm_lang->make_ctags = perl_make_ctags;
	pm_lang->internal = true;

	ENV->listOptions->add ("--perl");
	ENV->listOptions->add ("--files");

	ENV->modding->add (LPCHAR (pm));
	ENV->modding->add (LPCHAR (pm_lang));
	ENV->modding->add (LPCHAR (mod_perlfiles));
	
	return 0; 
}

