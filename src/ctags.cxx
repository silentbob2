/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <bugs.h>
#include <errno.h>
#include <string>

void make_ctags_fork ()
{
	std::string bob_cmd;
	char m_buf[512];
	int i;
	__djob_t * j;
	
	split_tmp_files ();
	bob_cmd = "silent_bob ";
	if (EQ (ENV->language, "C++"))
		bob_cmd += "--lang cpp ";

	for (i = 0; i < ENV->max_proc; ++i) {
		j = ENV->proc_list->fork ();
		if (j->child) {
			sprintf (m_buf, "%s -L %s%i -ts --kinds a >%s%i", bob_cmd.c_str (),
					ENV->tmp_files, i, ENV->tmp_tags, i);
			exit (execlp ("sh", "sh", "-c", m_buf, NULL));
		}
	}

	if (SB_FLGET (SB_FLVERBOSE)) {
		printf ("%s", "waiting threads...\n"); 
		fflush (stdout);
	}

	while ((j = ENV->proc_list->wait_all ()) && j) 
		Dexec_done (j);

	for (i = 0; i < ENV->max_proc; ++i) {
		sprintf (m_buf, "cat %s%i >>%s", ENV->tmp_tags,
				i, ENV->tmp_tags);
		system (m_buf);
	}
}

void make_ctags (EArray * d_files)
{
	std::string bob_cmd;
	char m_buf[1024];
	int i;

	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);

	bob_cmd = "silent_bob ";
	if (EQ (ENV->language, "C++"))
		bob_cmd += "--lang cpp ";

	if (! d_files || d_files->get_size () == 0) 
		find_cfiles ();
	else {
		if (d_files->strings_to_file (ENV->tmp_files) <= 0)
			return;
	}

	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		sprintf (m_buf, "cp ./tags %s", ENV->tmp_tags);
		system (m_buf);
	} else 
		unlink ("./tags");
	
	if (ENV->max_proc > 1) 
		make_ctags_fork ();
	else {
		sprintf (m_buf, "%s -L %s -ts --kinds a >>%s ", bob_cmd.c_str(),
				ENV->tmp_files, ENV->tmp_tags);
		if (SB_FLGET (SB_FLDEBUG))
			strcat (m_buf, "--debug ");
		i = system (m_buf);
		if (i != 0)
			bug_system ();	
	}

	mk_tags ((char *) "tags", NULL);
	remove_tmp_files ();
}

