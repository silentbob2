/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <dlfcn.h>
#include <head.h>
#include <mod.h>
#include <the_tt.h>
#include <dbg.h>
#include <bugs.h>

typedef struct DArray *(*plug_init_t) (struct env_t *env);

mod_t * findPluginID (int ID)
{
	int i;
	mod_t * plug;

	for (i = 0; i < ENV->modding->get_size (); i++)	{
		plug = (mod_t *) ENV->modding->get (i);
		if (plug->ID == ID)
			return plug;
	}

	return NULL;
}

char modding_optparse (int * i, int step)
{
	int pos;
	mod_t * plug;

	for (pos = 0; pos < ENV->modding->get_size (); pos++) {
		plug = (mod_t *) ENV->modding->get (pos);

		if (step == 1) {
			if (plug->opt && plug->opt (ENV->d_opts, i))
				return 1;
		}
		
		if (step == 2) {
			if (plug->opt2 && plug->opt2 (ENV->d_opts, i))
				return 1;
		}
	}

	return 0;
}

mod_t * find_language (char *S)
{
	mod_t * plug;
	int i;
	
	for (i = 0; i < ENV->modding->get_size (); i++) {
		plug = (mod_t *) ENV->modding->get (i);
		if (! plug->language)
			continue;
		if (EQ (plug->language, S))
		       break;
	}

	if (i < ENV->modding->get_size ())
		return plug;

	return NULL;
}	

void mods_short_info ()
{
	struct mod_t *pm = NULL;
	int i;

	printf ("Available plugins: \n");
	for (i = 0; i < ENV->modding->get_size (); i++) {
		pm = (mod_t *) ENV->modding->get (i);
		if (! pm)
			continue;

		if (! pm->short_info)
			continue;

		if (pm->internal)
			continue;

		printf ("[%d]\t", pm->ID);
		pm->short_info ();
		printf ("\n");	
	}
}

void plugins_viewer ()
{
	char m_buf[256];
	mod_t * plug;
	int id;

	while (true) {
		printf ("\n");
		mods_short_info ();
		printf ("Plugin id: ");
		fflush (stdout);
		if (!fgets (m_buf, 255, stdin))
			break;

		chomp (m_buf);
		if (EQ (m_buf, "q"))
			exit (0);

		id = atoi (m_buf);
		plug = findPluginID (id);
		if (! plug) {
			printf ("Bad plugin ID !\n");
			continue;
		}

		if (! plug->long_info) {
			printf ("No plugin info !\n");
			continue;
		}

		printf ("\n");
		plug->long_info ();
		printf ("\n<enter>\n");
		fflush (stdout);
		fgets (m_buf, 255, stdin);
		printf ("\n");
	}
}

void mod_plugin_info (int ID)
{
	mod_t * plug;

	plug = findPluginID (ID);
	if (! plug) {
		fprintf (stderr, "No such plugin [%d]", ID);
		exit (1);
	}

	if (plug->long_info)
		plug->long_info ();
}

FILE * mkctags_prepare ()
{
	FILE * ofile;
	
	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		if (rename ("./tags", ENV->tmp_tags) < 0) {
			fprintf (stderr, "$ rename: ./tags %s\n", ENV->tmp_tags);
			perror ("rename");
			exit (1);
		}
		ofile = fopen (ENV->tmp_tags, "a");
	} else {
		unlink (ENV->tmp_tags);
		unlink ("./tags");
		ofile = fopen (ENV->tmp_tags, "w");
	}

	if (ofile == NULL) {
		fprintf (stderr, "fopen (\"%s\", \"w\")\n", ENV->tmp_tags);
		perror ("fopen");
		exit (1);
	}

	return ofile;
}

int modding_start (int i_cmd)
{
	mod_t * plug;
	struct tt_state_t * tt;
	int i;
	int size;
	FILE * ofile = NULL;

//	printf ("Find language plugin ...\n"); fflush (stdout);
	plug = find_language (ENV->language);
	if (plug == NULL) 
		return 1;

	if (i_cmd == cmd_makectags) 
		ofile = mkctags_prepare ();
					
	size = ENV->d_files->get_size ();
	if (! size) {
		switch (i_cmd) {
			case cmd_makectags:
				if (! plug->make_ctags)
					return 2;
				plug->make_ctags (NULL, ofile);				
				break;

			case cmd_tags:
				printf ("No such tags !\n");
				exit (0);
				break;
		}
		goto mod_out;
	}

	if (i_cmd == cmd_tags) {
		if (! plug->print_tags)
			return 2;
		else {
			plug->print_tags ();
			return 0;
		}
	}

	for (i = 0; i < size; i++) {
		if (S_ISDIR (DLSTAT (ENV->d_files->get (i))->st_mode)) 
			continue;
		
		switch (i_cmd) {
			case cmd_the_tt:
				tt = CNEW (tt_state_t, 1);
				memset (tt, 0, sizeof (struct tt_state_t));
				tt->fileName = ENV->d_files->get (i);

				if (! plug->the)
					return 2;

				plug->the (tt);
				if (! SB_FLGET (SB_FLSIMULATE)) {
					write (fileno (stdout), tt->result, 
						tt->resultSize);
					printf ("\n");
				}

				free_tt_state (tt);
				break;

			case cmd_makectags:
				if (! plug->make_ctags)
					return 2;

				plug->make_ctags (ENV->d_files->get (i), ofile);
				break;

			case cmd_call_tags:
				if (! plug->call_tags)
					return 2;

				plug->call_tags (ENV->d_files->get (i));
				break;

			case cmd_file:
				if (! plug->file)
					return 2;
				plug->file (ENV->d_files->get (i));
				break;

			default:
				bug_notsupported ();
				i = size;
				break;
		}
	}	

mod_out:
	if (ofile)
		fclose (ofile);

	if (i_cmd == cmd_makectags) 
		mk_tags ((char *) "tags", NULL);

	return 0;
}

int modding_load_dir (char * path)
{
	int i;
	DArray * files;
	
	files = Dfiles (path);
	if (! files)
		return -1;
	
	for (i = 0; i < files->get_size (); i++) {
		if (EQ (files->get (i), "."))
			continue;
		if (EQ (files->get (i), ".."))
			continue;
		modding_load_plugin (files->get (i), path);
	}		

	files->foreach (free);
	delete files;
	return 0;
}

int modding_init ()
{
	ENV->modding = new EArray(32);
	modding_load_dir (ENV->home_plugins);
	modding_load_dir (ENV->shared_plugins);
	return 0;
}

int modding_load_plugin (char * name, char * path)
{
	int n;
	struct stat st;
	char * s_dlerror;
	void * lib_handle;
	DArray *plug_list;
	struct mod_t * sb_plugin;
       	plug_init_t func_handle;
	char m_buf[512];

	if (! path || ! name)
		return -1;

	if (*stail (path) == '/')
		*stail(m_buf) = '\0';

	m_buf[511] = 0;
	snprintf (m_buf, 511, "%s/%s", path, name);

	stat (m_buf, &st);
	if (! S_ISREG (st.st_mode))
		return -1;
	
	dlerror ();
	lib_handle = dlopen (m_buf, RTLD_NOW);
	s_dlerror = (char *) dlerror ();
	if (s_dlerror) {
		bug_plugin (s_dlerror);
		return -1;
	}

	func_handle = (plug_init_t) dlsym (lib_handle, "plugin_init");
	s_dlerror = (char *) dlerror ();
	if (s_dlerror) {
		bug_plugin (s_dlerror);
		return -1;
	}

	plug_list = func_handle (ENV);
	if (plug_list == NULL)
		return -1;
	
	for (n = 0; n < plug_list->get_size (); n++) {
		sb_plugin = (struct mod_t *) plug_list->get (n);
                sb_plugin->mod_file = strdup (m_buf); 
		if (sb_plugin->short_info)
			sb_plugin->ID = newPluginID ();
		if (sb_plugin) 
			ENV->modding->add (LPCHAR (sb_plugin));
	}

	return 0;
}

