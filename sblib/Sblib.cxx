/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <env.h>
#include <bob_flags.h>
#include <head.h>
#include <wit.h>
#include <dbg.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <the_tt.h>

struct env_t *ENV;

int sb_prname (char *arg)
{
	int i_cmd = 0;
	char * d_ptr;
	d_ptr = arg + strlen (arg);
	d_ptr--;
	
	while (*d_ptr != '\\' &&
			d_ptr > arg)
		d_ptr--;

	if (*d_ptr == '\\') 
		d_ptr++;
	
	if (EQ (d_ptr, "bob_perl"))
		ENV->language = strdup ("Perl");
	else if (EQ (d_ptr, "bob_python"))
		ENV->language = strdup ("Python");
	else if (EQ (d_ptr, "bob_ruby"))
		ENV->language = strdup ("Ruby");

	if (EQ (d_ptr, "tags"))
		i_cmd = cmd_tags;
	else if (EQ (d_ptr, "the_tt"))
		i_cmd = cmd_the_tt;
	else if (EQ (d_ptr, "gc_indent"))
		i_cmd = cmd_indent;
	else if (EQ (d_ptr, "structs"))
		i_cmd = cmd_give_structs;

	return i_cmd;
}

// SilentBob --tags
void tags (DArray * d_names, char * d_file_output)
{
	struct fdump_param_t d_param;
	EArray * d_tags;			
	struct d_tag_t * d_tag;
	int a, i, n_names;
	
	if (! d_names)
		return;
	
	n_names = d_names->get_size ();
	
	for (i = 0; i < n_names; i++) {
		d_tags = got_tag (d_names->get (i));
		if (d_tags == NULL) {
			if (d_tags->get_size () == 0) {
				fprintf (ENV->d_stream_dbg, "Tag not found : %s\n",
					       	d_names->get (i));
				fflush (ENV->d_stream_dbg);
				delete d_tags;
				continue;
			}
		}
		
		if (! d_tags)
			continue;

		if(! d_tags->get_size ()) {
			delete d_tags;
			d_tags = NULL;
			continue;
		}
		
		fault (! d_tags);

		for (a = 0; a < d_tags->get_size (); a++) {
			d_tag = (d_tag_t *) d_tags->get (a);

			fault (! d_tag);
			
			if (! d_file_output)
				printf ("// file %s line %i\n",
						d_tag->d_file, d_tag->d_line);
			
			memset (&d_param, 0, sizeof (struct fdump_param_t));
			d_param.n_trip = 0;
			d_param.d_file_name = d_tag->d_file;
			d_param.d_line = d_tag->d_line;
			d_param.linear = true;
			d_param.d_file_output = d_file_output;
			if (d_tag->d_type == OT::Function)
				d_param.b_force_block = true;
			
			nogui_fdump (&d_param);
			if (! d_file_output)
				fputc ('\n', stdout);
	
			DROP (d_tag);				
		}
		
		if (d_tags) {
			d_tags->drop ();
			delete d_tags;
		}
	}
	
	fflush (stdout);
}

void bug_longmacro ()
{
	printf ("Too big macro."
			"If your macro have more than 300 lines, please "
			"contact <graycardinal@pisem.net>\n"
			"Program stopped.\n");
	
	exit (0);
}

void bug_nosuch_tag (char * f_name)
{
	printf ("Tag \"%s\" not found. Broken \"tags\" file ? "
			"Try \"silent-bob --make-ctags\".\n", f_name);
}

void bug_nocalltags ()
{
	printf ("File \"call_tags\" not found. "
			"Try \"silent-bob --call-tags [-L] <files>\"\n");
	exit (1);
}

void bug_system ()
{
	printf ("Can't make tags file. Maybe you do not have write permissions ?\n");
	exit (1);
}

void bug_fork ()
{
	perror ("fork");
	exit (1);
}

void bug_plugin (char *name)
{
	fprintf (stderr, "Can't load plugin (%s)\n", name);
}

void bug_notsupported ()
{
	printf ("SilentBob (or language plugin)"
	       " don't support this feature !\n");
}

bool bob_tag (char *d_str, char * d_name, d_tag_t * d_tag)
{
	char m_buf[256];
	char *d_file;
	char *S;

	strcpy (m_buf, d_str);
	d_str = m_buf;
	
	strncpy (d_tag->d_name, d_name, 255);
	d_tag->d_name[255] = 0;
	S = strchr (d_str, '\t');
	if (! S)
		return false;

	S++;
	d_file = S;
	S = strchr (d_file, '\t');
	if (! S)
		return false;
	
	*S = 0;
	strncpy (d_tag->d_file, d_file, 255);
	d_tag->d_file[255] = 0;
	
	S++;

	if (if_digit (S)) 
		d_tag->d_line = atoi (S);
	else 
		return false;

	return true;
}

char * cts (struct c_word * d_word)
{
	char * S;

	if (d_word == NULL)
		return NULL;

	S = d_word->S;
	if (!strncmp (S, "else ", 5))
		S += 5;
	
	if (d_word->ch != '(')
		return NULL;
	
	while (!strncmp (S, "do ", 3))  
		S += 3;
	
	if (!strncmp (S, "return ", 7))
		S += 7;
	
	if (ENV->d_cops->sfind (S) != -1)
		return NULL;
	
	if (words_count (S) != 1)
		return NULL;
	
	return S;
}

bool def_test (char * d_op)
{
	char * S = d_op;
	++S;
	while (*S == ' ' || *S == '\t')
		++S;

	if (! strncmp (S, "define", 6))
		return true;
	return false;
}

int find_cfiles ()
{
	find_one_ext ((char *) "*.h");
	find_one_ext ("*.hpp");
	find_one_ext ("*.cpp");
	find_one_ext ("*.c");
	find_one_ext ("*.cc");
	find_one_ext ("*.cxx");
        return 0;
}

int sblib_find (const char * path, const char * name, const char * f_outname)
{
	int devnull;
	int fd;
	int pid;
	int status = -1;

	pid = fork ();
	if (pid == 0) {
		devnull = open ("/dev/null", O_WRONLY, 0600);
		fd = open (f_outname, O_WRONLY, 0600);
		if (fd == -1) {
			fd = open (f_outname, O_WRONLY | O_CREAT, 0600);
			if (fd == -1) {
				close (devnull);
				return -1;
			}
		} else
			lseek (fd, 0, SEEK_END);
		dup2 (devnull, 2);
		dup2 (fd, 1);
		execlp ("find", "find", "-name", name, NULL);
	} else if (pid > 0) {
		waitpid (pid, &status, 0);
		return status;
	}

	return status;		
}

# define TMP_FILE_NAME (char *) "./silent_bob.tmp"
struct tt_state_t * get_onett_tag (char * f_name, char ** d_tt_buf) 
{
	DArray d_array;
	tt_state_t * Ret = NULL;
	char * S;

	if (d_tt_buf)
		*d_tt_buf = NULL;	
	
	d_array.add (f_name);
	Ret = CNEW (tt_state_t, 1);

	unlink (TMP_FILE_NAME);
	tags (&d_array, TMP_FILE_NAME);

	while( 1 ) {
		if (access (TMP_FILE_NAME, R_OK) != 0)
			break;
	
		Ret->fileName = strdup (TMP_FILE_NAME);
		S = THE_TT::do_tt_file (Ret);
	
		if (S == NULL)
			break;
	
		if (Ret->mmaped)
			munmap (Ret->fileData, Ret->fileDataSize);

		if (Ret->fd)
			close (Ret->fd);

		if (d_tt_buf)
			*d_tt_buf = S;

		unlink (TMP_FILE_NAME);
		return Ret;
	}

	DROP (Ret);
	unlink (TMP_FILE_NAME);
	return NULL;	
}

void globalsPrint (tt_state_t * tt, char * d_out, int d_found_type)
{
	int t_line;
	char * S;	

	if (SB_FLGET (SB_FLSIMULATE))
		return;
	
	if (d_found_type & OT::Other)
		return;

	t_line = tt->attachment[ENV->t_op_no].pair_line+1;
	t_line += ww_begin_line (tt, d_out, 
			tt->attachment[ENV->t_op_no].offset);
	
	if (SB_FLGET(SB_FLTAGSTYLE))
		mk_tag (d_out, tt->fileName, 
				t_line, d_found_type); 
	else {
		if (d_found_type & OT::Class) {
			S = strchr (d_out, ':');
			if (S) { 
				*S = '\0';
				if (S[-1] == ' ')
					S[-1] = '\0';
			}
		}
		printf ("%s\t\t//file %s //line %i\n", d_out, 
				tt->fileName, t_line);
	}
}

#define NOT_VALID 0
#define CALL_BACK 1
#define VALID 2

EArray * got_tag (char * d_tag)
{
	struct d_tag_t *d_new_tag;
	char d_buf[256];
	EArray * d_ret;
	int d_len;
	char *S;
	int i;

	d_ret = new EArray;

	if (ENV->d_tags_file == NULL) {
		ENV->d_tags_file = new EArray;
		ENV->d_tags_file->from_file ((char *) "./tags");	
	}

	snprintf (d_buf, 255, "%s\t", d_tag);
	d_len = strlen (d_buf);

	if (ENV->d_tags_file->get_size () == 0)
		return d_ret;
	
	i = ENV->d_tags_file->snfind_fast (d_buf, strlen (d_buf));
	
	if (i == -1) {
		fprintf (ENV->d_stream_dbg, "\tENV->d_tags_file->snfind_fast == -1");	LN;
		return d_ret;
	}
	
	d_len = strlen (d_buf);
	do {
		i++;
		S = ENV->d_tags_file->get (i);
		if (! S)
			break;
	} while (! strncmp (S, d_buf, d_len));
	--i;
	
	while (true) {
		S = ENV->d_tags_file->get (i);
		fprintf (ENV->d_stream_dbg, "\ttag : %s\n", S); fflush (ENV->d_stream_dbg);

		d_new_tag = CNEW (d_tag_t, 1);
		memset (d_new_tag, 0, sizeof (d_tag_t));
		if (strstr (S, ";\tf"))
			d_new_tag->d_type = OT::Function;

		if (bob_tag (S, d_tag, d_new_tag) == false) {
			DROP (d_new_tag);
			fprintf (ENV->d_stream_dbg, "\tBAD tag : %s\n", S);
			return d_ret;
		}
		
		d_ret->add ((long) d_new_tag);
		
		i--;
		if (i < 0)
			break;
		
		S = ENV->d_tags_file->get (i);
		if (strncmp (S, d_tag, strlen (d_tag)))	 	
			break;
	}	

	return d_ret;
}

char * macro_name (char * d_op, char * d_macro_name)
{
	char *d_begin;
	char *S;
	char m_buf[256];

	strncpy (m_buf, d_op, 255);
	m_buf[255] = 0;
	
	S = strstr (m_buf, "define");
	
	if (! S)
		return NULL;
	
	S = strchr (S, ' ');
	if (! S)
		return NULL;
	
	while (*S == ' ')
		S++;

	d_begin = S;
	S = strchr (d_begin, ' ');
	if (S)
		*S = 0;

	S = strchr (d_begin, '(');
	if (S)
		*S = 0;
	
	strcpy (d_macro_name, d_begin);
	return d_macro_name;
}

void mk_tag_macro (char * d_op, char * d_file, int t_line)
{
	char S[256];
	
	if (! macro_name (d_op, S))
		return;
	
	printf ("%s\t%s\t%i\n", S, d_file, t_line);
}

void mk_tag (char * d_op, char * d_file, int line, int d_found_type)
{
	char * d_format = (char *) "%s\t%s\t%i\n";
	char * S;
	
	if (d_found_type & OT::Define) {
		mk_tag_macro (d_op, d_file, line);
		return;
	}

	if (d_found_type & OT::Class) {
		S = strchr (d_op, ':');
		if (S) 
			*S = 0;		
	}
	
	S = ww_last_word (d_op);
	if (! S || !(*S) || (*S == ' '))
		return;

	if (*S == 's') {
		if (! strncmp (S, "static", 6))
			return;
		if (! strncmp (S, "struct", 6))
			return;
	}
	
	if (*S == 'u' && !strncmp (S, "union", 5))
		return;
	if (*S == 'e' && !strncmp (S, "enum", 4))
		return;	
		
	if (d_found_type & OT::Function)
		d_format = (char *) "%s\t%s\t%i\t;\tf\n";
	else if (d_found_type & OT::Class)
		d_format = (char *) "%s\t%s\t%i\t;\tc\n";
	else if (d_found_type & OT::Struct)
		 d_format = (char *) "%s\t%s\t%i\t;\ts\n";

	printf (d_format, S, d_file, line);
}

DArray * mk_tag_structtail_split (char *S)
{
	bool b_macro = false;
	char * d_old = NULL;	
	int brace_depth = 0;
	DArray * d_array;

	d_array = new DArray (128);
	if (! d_array)
		return NULL;
	
	d_old = S;
	
	while (true) {		
		if (*S == '\"' || *S == '\'') {
			S = sstrend (S);
			if (S == NULL || *S == '\0')
				break;
		}

		if (*S == '(')
			brace_depth++;
		
		if (*S == ')') {
			brace_depth--;
			if (! brace_depth) {
				S++;
				break;
			}
		}			
		
		if (*S == 0) {
			if (! b_macro)
				d_array->add (d_old);
			break;
		}
		
		if (brace_depth) {
			S++;
			continue;
		}
	
		if (S[1] == '(') {
			b_macro = true;
			S++;
			continue;
		}			
			
		if (*S == ' ' || *S == ',') {
			*S = 0;
			S++;
			b_macro = false;
			while (*S == ' ' || *S == '*')
				S++;
			if (! b_macro)
				d_array->add (d_old);
			d_old = S;
			
			continue;
		}
		S++;
	}

	return d_array;
}

void mk_tag_structtail (char * S, char * d_file, int t_line)
{
	char *w;
	DArray * d_array;
	int i;

	d_array = mk_tag_structtail_split (S);
	if (! d_array) 
		return;
	
	for (i = 0; i < d_array->get_size (); i++) {
		w = ww_last_word (d_array->get (i));
		if (! w || !(*w) || *w == ' ')
			continue;
		printf ("%s\t%s\t%i\n", w, d_file, t_line);
	}

	delete d_array;		
}

void mk_tags (char *f_name, DArray *d_in)
{
	DHeapSort * heap;
	DArray * d_array = NULL;
	char *S;
	FILE * my_file;
	int d_size;
	int i;

	if (d_in == NULL) {
		d_array = new DArray (1024);
		d_array->from_file (ENV->tmp_tags);
	} else 
		d_array = d_in;

	d_size = d_array->get_size ();
	heap = new DHeapSort (d_size);

	for (i = 0; i < d_size; ++i) 
		heap->add (d_array->get (i));

	my_file = fopen (f_name, "w");
	if (my_file == NULL) {
		fprintf (stderr, "file %s:\n", f_name);
		perror ("fopen");
		return;
	}

	fprintf (my_file, "!_TAG_FILE_FORMAT\t2\n");
	fprintf (my_file, "!_TAG_FILE_SORTED\t1\n");
	fprintf (my_file, "!_TAG_PROGRAM_AUTHOR\tOleg Puchinin (graycardinalster@gmail.com)\n");
	fprintf (my_file, "!_TAG_PROGRAM_NAME\tSilent Bob\n");
	fprintf (my_file, "!_TAG_PROGRAM_URL\thttp://sf.net/projects/silentbob\n");
	fprintf (my_file, "!_TAG_PROGRAM_VERSION\t1.6\n");

	while ((S = heap->extract_min ()) && S) 
		fprintf (my_file, "%s", S);
		
	if (d_in == NULL) {
		d_array->foreach (free);
		delete d_array;
	}

	fclose (my_file);
	delete heap;
}

char * name2obj (char * name)
{
	char *S;
	char m_buf[512];
	if (! name)
		return NULL;
	strcpy (m_buf, name);
	S = rindex (m_buf, '.');
	if (! S)
		return NULL;
	strcpy (S, ".o");
	return strdup (m_buf);	
}
bool b_in_comment;

bool brace_count (char * d_str, int * d_count, bool b_force_block) // "nice"
{
	bool Ret = false;

	if (! d_str || ! d_count)
		return false; 
	
	while (*d_str != 0) {	
		if (!strncmp (d_str, "/*", 2)) {
			b_in_comment = true;
			d_str += 2;
			continue;
		}
		
		if (b_in_comment) {
			if (strncmp (d_str, "*/", 2)) {
				d_str ++;
				continue;
			} else {
				d_str += 2;
				b_in_comment = false;
				continue;
			}
		}		
		
		if (!strncmp (d_str, "//", 2)) 
			break;

		if (*d_str == '\"' || *d_str == '\'') {
			d_str = sstrend (d_str);
			if (d_str == NULL || *d_str == 0) {
				assert (true, "HimTeh 4");
				break;
			}
		}
		
		if (*d_str == '{') {
			Ret = true;
			(*d_count)++;
		}
		
		if (*d_str == '}') {
			Ret = true;
			(*d_count)--;
		}

		if (*d_str == ';' && *d_count == 0 && !b_force_block) {
			Ret = true;
			break;
		}
		d_str++;
	}
	
	return Ret;
}

void nogui_fdump (struct fdump_param_t * d_param)
{
	int d_count = 0;
	DArray d_array;
	FILE * d_file;
	int d_size;
	char * S;
	int i,a;

	if (!d_array.from_file (d_param->d_file_name))
		return;

	if (d_param->d_file_output == NULL)
		d_file = stdout;
	else
		d_file = fopen (d_param->d_file_output, "w");
	
	if (! d_file)
		return;
	
	if (! d_param->linear) {
		for (a = 0; a	< d_param->n_trip; a++)
			fprintf (d_file, "\t");
		fprintf (d_file, "//<***>\n");
	}
	
	i = d_param->d_line-1;
	d_size = d_array.get_size ();
	b_in_comment = false;
	
	if (d_array.get (i)[0] != '#') {
		while (i < d_size) {
			if (!d_param->linear) {
				for (a = 0; a	< d_param->n_trip; a++)
					fprintf (d_file, "\t");
			}
			fprintf (d_file, "%s", d_array.get(i));
				
			if (brace_count (d_array.get(i), &d_count, d_param->b_force_block) && !d_count)
				break;
			
			if (!d_count && ((i - d_param->d_line) > 2) && !d_param->b_force_block)
				break;
			
			i++;
		}
	} else {
		do {
			S = d_array.get (i);
			fprintf (d_file, "%s", S);
			S = &S[strlen (S)-2];
			while ((*S == ' ') || (*S == '\t'))
				S--;
			
			if (*S != '\\')
				break;
			i++;
		} while (i < d_size);
	}
	
	if (!d_param->linear) {
		for (a = 0; a	< d_param->n_trip; a++)
			fprintf (d_file, "\t");
		fprintf (d_file, "//</***>\n");
	}

	if (d_param->d_file_output != NULL)
		fclose (d_file);

	d_array.foreach (free); 

}

/* code for "linear" functionality, */
void nogui_tagsdump (char * f_name, int n_trip) {
	DArray * d_tags;			
	d_tag_t * d_tag;
	struct fdump_param_t d_param;
	int a,i;
	
	d_tags = got_tag (f_name);
	
	assert (d_tags->get_size () == 0, "HimTeh 1");
	for (i = 0; i < d_tags->get_size (); i++) {
		d_tag = (d_tag_t *) d_tags->get (i);
		if (i != 0)
			fputc ('\n', stdout);

		if (!SB_FLGET(SB_FLLINEAR)) {
			for (a = 0; a < n_trip; a++)
				fputc ('\t', stdout);
		}

		printf ("// file %s line %i\n", d_tag->d_file, d_tag->d_line);
		
		memset (&d_param, 0, sizeof (struct fdump_param_t));
		d_param.n_trip = n_trip;
		d_param.d_file_name = d_tag->d_file;
		d_param.d_line = d_tag->d_line;
		d_param.linear = SB_FLGET (SB_FLLINEAR);
		if (d_tag->d_type & OT::Function)
			d_param.b_force_block = true;
		nogui_fdump (&d_param);
	}
	
	d_tags->foreach (free);
	delete d_tags;
	printf ("\n");
}

void opMacro (char ** d_ptr, char ** d_out, char ch)
{
	char *macro_start;
	char * d_my;
	int n = 0;
	
	d_my = *d_out;
	macro_start = *d_out;
	while (true) {
		if (ch == '\n')
			n++;

		if ((ch == '\n') && (d_my[strlen (d_my) - 1] != '\\'))
			break;

//		if (n > 300) 
//			break;

		ch = t_op (d_ptr, d_out);
		ENV->t_op_no++;		
		if (ch == 0)
			break;
		d_my = *d_out;
	}
}

int remove_tmp_files ()
{
	char m_buf[512];
	int i = 0;

	for (i = 0; i < ENV->max_proc; ++i) {
		sprintf (m_buf, "%s%i", ENV->tmp_files, i);
		unlink (m_buf);
	}	
	
	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);
	return ENV->max_proc;
}

void setParam (char *opt)
{
	char m_buf[512];
	char *S;

	if (! opt)
		return;
	strcpy (m_buf, opt);
	S = index (m_buf, '=');
	if (! S)
		return;
	
	*S = '\0';
	++S;
	strip2 (m_buf);
	strip (S);
	ENV->settings->set (m_buf, strdup (S));	
}

int split_tmp_files ()
{
	FILE * f_tmpfiles;
	FILE ** proc_files;
	char m_buf[512];
	int i = 0;

	f_tmpfiles = fopen (ENV->tmp_files, "r");
	if (! f_tmpfiles)
		return -1;
	
	proc_files = CNEW (FILE *, ENV->max_proc);
	memset (proc_files, 0, sizeof (FILE *) * ENV->max_proc);
	errno = 0;
	for (i = 0; i < ENV->max_proc; ++i) {
		sprintf (m_buf, "%s%i", ENV->tmp_files, i);
		unlink (m_buf);
		proc_files[i] = fopen (m_buf, "w");
		if (! proc_files[i]) {
			perror ("fopen");
			return -1;
		}
	}	
	
	i = 0;
	while (fgets (m_buf, 512, f_tmpfiles)) {
		fprintf (proc_files[i], "%s", m_buf);
		if (++i >= ENV->max_proc)
			i = 0;		
	}
	
	for (i = 0; i < ENV->max_proc; ++i) 
		fclose (proc_files[i]);
	
	return ENV->max_proc;
}

DArray * split_to_words (char * d_op)	
{
	DArray * d_Ret = new DArray (16);
	char * d_old = strdup (d_op);
	bool b_done = false;
	char * S = d_old;
	char * d_end;
	char ch;
	
	if (d_Ret == NULL || d_old == NULL)
		return NULL;
	
	while (true) {
		b_done = false;
		d_end = S;
		
		if (*d_end == ' ')
			d_end++;
		
		while (*d_end) {
			if (!(if_abc(d_end) ||
				  if_digit (d_end) ||
				  *d_end == '_' || 
				  *d_end == ' ') )
				break;
			d_end ++;
		}

		if (! *d_end) {
			ch = 0;
			b_done = true;
			goto split_to_words_L1;
		}

		ch = *d_end;
		if (d_end[-1] == ' ')
			d_end[-1] = 0;
		else 
			*d_end = 0;

		while (*S && *S == ' ')
			S++;
		
split_to_words_L1:
		d_Ret->add (LPCHAR(new_cword (S, ch)));

		if (b_done)
			break;
		
		if (ch == '\"' || ch == '\'') {
			*d_end = ch;
			d_end = sstrend (d_end);
			assert (d_end == NULL, "Lena 1");
			if (*d_end == '\0' || *(++d_end) == '\0')
				break;
		}
		
		S = d_end + 1;
	}

	DROP (d_old);	
	return d_Ret;
}

char * sstrend (char * d_ptr)
{
	bool t_instring = false;
	int d_slash_count;
	char ch_last;
	char *d_old;
	unsigned limit = 1024;
	
	if (! d_ptr)
		return (char *) 0;

	if (!(*d_ptr))
		return (char *) 0;
	
	ch_last = *d_ptr;
	d_old = d_ptr;
	limit--;
	while (*d_ptr && (limit > 0)) {
		if (*d_ptr == '\'' || *d_ptr == '\"') {
			if (t_instring && *d_ptr != ch_last) {
				d_ptr++;
				continue; // Mmm...
			}
			
			if (t_instring) {
				if (d_ptr[-1] == '\\') {
					d_slash_count = 1;
					while (d_ptr [-(d_slash_count)] == '\\')
							d_slash_count++;
				
					if (d_slash_count & 1) 
						t_instring = false;
				} else {
					d_ptr++;
					t_instring = false;
					continue;
				}
			} else {
				ch_last = *d_ptr;
				t_instring = true;
			}
		}
		
		if (t_instring)	{
			d_ptr++;
			continue;
		} else
			break;
	}

	d_ptr --;
	
	if (*d_ptr == 0)
		return 0;

	return d_ptr;
}

char * sstrkill (char *OP)
{
	char *S;
	char *tail;

	if (! OP)
		return NULL;

	S = OP;
	while (*S) {
		if (*S == '\"' || *S == '\'') {
			tail = sstrend (S);
			if (! tail)
				break;

			if (*tail == '\0' ||*(tail+1) == '\0') {
				*S = '\0';
				break;
			}

			++S;
			strcpy (S, tail);
		}
		++S;
	}
	
	return OP;
}

int words_count (char *S)
{
	bool b_begin = true;
	int d_ret = 0;

	if (S == 0)
		return 0;
	
	while (*S) {
		if (*S == ' ') {
			b_begin = true;
			S++;
			continue;
		}
				
		if (b_begin) {
			if (if_abc (S) ||
					(*S == '_') ||
					(*S == '*') ||
					(*S == '&')) {
				S++; 
				d_ret ++; 
				b_begin = false;
				continue;
			} else
				break;
		} else {
			if (!(if_abc (S) || (*S == '_') 
				|| (*S == '*') || (if_digit (S))))
				break;
		}
		
		S++;
		b_begin = false;
	}

	return d_ret;	
}

int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->fileData [d_offset] - 1;
	char * d_end = &d_out[strlen (d_out)] - 1;
	int Ret = 0;

	while (d_end > d_out) {		
		if (*d_end == ' ' || *d_end == '\t') {
			while ((S >= d_tt_state->fileData) && (*S == ' ' || *S == '\t'))
				--S;
			
			if (S < d_tt_state->fileData)
				return Ret;

			while ((d_end >= d_out) && (*d_end == ' ' || *d_end == '\t'))
				--d_end;

			if (d_end <= d_out)
				return Ret;

			continue;
		}
		
		if (*S == '\n')
			--Ret;

		if (*S == *d_end)
			--d_end;

		--S;
	}

	return Ret;
}

char * ww_begin_offset (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->fileData [d_offset] - 1;
	char * d_real = &d_out[strlen (d_out)] - 1;

	while (d_real != d_out) {		
		if (*d_real == ' ' || *d_real == '\t') {
			while (*S == ' ' || *S == '\t')
				S--;
			
			while ((*d_real == ' ' || *d_real == '\t') 
					&& (d_real != d_out))
				--d_real;
			
			continue;
		}
		
		if (*S == *d_real)
			--d_real;

		--S;
	}

	return S;
}

char * ww_last_word (char *d_op)
{
	char * S = d_op;
	char * d_word;
	
	while (*S) {
		if (*S == '(' || *S == '=' || *S == '[')
			break;
		S++;
	}

	if (S[-1] == ' ')
		S--;

	*S = 0;
	d_word = d_op;
	while (true) {
		S = strchr (d_word, ' ');
		if (S == NULL)
			break;
		d_word = S+1;
	}

	while (*d_word == '*' ||
			*d_word == '&' ||
			*d_word == ' ')
		d_word++;
	
	return d_word;
}

char * ruby_last_word (char * op)
{
	char * d_word;
	char * S;
	
	S = op;
	while (*S) {
		if (*S == '(' || *S == '=' || *S == '[' || *S == '<')
			break;
		S++;
	}

	while (S[-1] == ' ')
		S--;

	*S = 0;
	d_word = op;
	while (true) {
		S = strchr (d_word, ' ');
		if (S == NULL)
			break;
		d_word = S+1;
	}

	return d_word;
}

bool validOption (char *Name)
{
	char *S;
	if (! Name)
		return false;

	S = ENV->listOptions->first ();
	while (S) {
		if (EQ (Name, S))
			return true;
		S = ENV->listOptions->next ();
	}
	return false;
}

bool builtin_language ()
{
	if (EQ (ENV->language, "C") ||
			EQ (ENV->language, "C++") ||
			EQ (ENV->language, "Perl") ) 
		return true;
	return false;
}

int lastPluginID = 0;
int newPluginID ()
{
	return ++lastPluginID;
}


