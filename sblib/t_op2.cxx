/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

char t_op2 (char ** d_in, char ** d_prev)
{
	char *d_ptr = *d_in;
	char ch = 0;
	
	if (*d_prev)
		*d_prev = *d_in;

	while (*d_ptr) {
		if (*d_ptr == '{' || *d_ptr == ';' ||
				*d_ptr == '}') {
			if (d_ptr[-1] == '\\') {
				++d_ptr;
				continue;
			}
			ch = *d_ptr;
			*d_ptr = 0;
			++d_ptr; // space
			break;
		}
		++d_ptr;
	}

	++d_ptr;
	*d_in = d_ptr;
	return ch;
}

